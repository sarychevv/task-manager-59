package ru.t1.sarychevv.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.comparator.CreatedComparator;
import ru.t1.sarychevv.tm.comparator.NameComparator;
import ru.t1.sarychevv.tm.comparator.StatusComparator;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;

import java.util.Comparator;

public enum ProjectSort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<ProjectDTO> comparator;

    ProjectSort(@NotNull final String displayName, @NotNull final Comparator<ProjectDTO> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static ProjectSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator<ProjectDTO> getComparator() {
        return comparator;
    }

}
