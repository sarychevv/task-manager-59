package ru.t1.sarychevv.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.project.*;
import ru.t1.sarychevv.tm.dto.response.project.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    String NAME = "ProjectEndpoint";

    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(@WebParam(name = REQUEST, partName = REQUEST)
                                                            @NotNull ProjectChangeStatusByIdRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                                  @NotNull ProjectChangeStatusByIndexRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectClearResponse clearProject(@WebParam(name = REQUEST, partName = REQUEST)
                                      @NotNull ProjectClearRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(@WebParam(name = REQUEST, partName = REQUEST)
                                        @NotNull ProjectCreateRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectGetByIdResponse getProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                          @NotNull ProjectGetByIdRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectGetByIndexResponse getProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull ProjectGetByIndexRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectListResponse listProject(@WebParam(name = REQUEST, partName = REQUEST)
                                    @NotNull ProjectListRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull ProjectRemoveByIdRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                      @NotNull ProjectRemoveByIndexRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull ProjectUpdateByIdRequest request) throws Exception;

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                      @NotNull ProjectUpdateByIndexRequest request) throws Exception;

}
