package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.dto.IDTORepository;
import ru.t1.sarychevv.tm.dto.model.AbstractModelDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;

import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> extends IDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    @NotNull
    M removeOneById(@Nullable String id) throws Exception;

}

