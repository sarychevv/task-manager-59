package ru.t1.sarychevv.tm.api.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.sql.SQLException;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    void close();

    @NotNull
    @SneakyThrows
    Liquibase getLiquibase() throws SQLException;
}
