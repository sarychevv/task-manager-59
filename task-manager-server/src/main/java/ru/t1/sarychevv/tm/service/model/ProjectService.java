package ru.t1.sarychevv.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.model.IProjectRepository;
import ru.t1.sarychevv.tm.api.service.model.IProjectService;
import ru.t1.sarychevv.tm.api.service.model.IUserService;
import ru.t1.sarychevv.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.model.Project;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    protected IProjectRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String userId,
                          @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setUser(userService.findOneById(userId));
        return repository.add(userId, project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(userService.findOneById(userId));
        return repository.add(userId, project);
    }

    @Override
    @Transactional
    public Project updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.update(userId, project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project updateByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final String name,
                                 @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.update(userId, project);
        return project;
    }

}

