package ru.t1.sarychevv.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.sarychevv.tm.api.service.dto.ISessionDTOService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;

@Service
@NoArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {

    @NotNull
    @Autowired
    private ISessionDTORepository repository;

    @NotNull
    protected ISessionDTORepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO updateById(@Nullable final String userId,
                                 @Nullable final String id,
                                 @Nullable final String name,
                                 @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final SessionDTO sessionDTO = findOneById(userId, id);
        if (sessionDTO == null) throw new TaskNotFoundException();
        sessionDTO.setName(name);
        sessionDTO.setDescription(description);
        repository.update(userId, sessionDTO);
        return sessionDTO;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO updateByIndex(@Nullable final String userId,
                                    @Nullable final Integer index,
                                    @Nullable final String name,
                                    @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final SessionDTO sessionDTO = findOneByIndex(userId, index);
        if (sessionDTO == null) throw new TaskNotFoundException();
        sessionDTO.setName(name);
        sessionDTO.setDescription(description);
        repository.update(userId, sessionDTO);
        return sessionDTO;
    }
}

