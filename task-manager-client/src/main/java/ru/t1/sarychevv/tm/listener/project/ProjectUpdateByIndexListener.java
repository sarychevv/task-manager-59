package ru.t1.sarychevv.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.project.ProjectUpdateByIndexRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class ProjectUpdateByIndexListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    @EventListener(condition = "@ProjectUpdateByIndexListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setDescription(description);
        request.setName(name);
        getProjectEndpoint().updateProjectByIndex(request);
    }

}
